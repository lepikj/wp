Role Name
=========

wordpress-install

Requirements
------------

* Databes should be available prior using this role.
* php cli is needed
* wp cli is also required.

Role Variables
--------------

wp_url: 192.168.8.112
wp_title: "Zö blög"
wp_admin: superloser
wp_pass: SotRoNandom790
wp_mail: e@mail.nowhere.zz

Dependencies
------------

uses variables from 
    https://github.com/geerlingguy/ansible-role-mysql.git
for database connection settings
and 
    https://github.com/sbaerlocher/ansible.wp-cli.git
can be used to get wp-cli installed.

Example Playbook
----------------

- hosts: all
  remote_user: root
  become: yes
  vars_files:
    - vars/mysql.yml
    - vars/wordpress.yml

  roles:
     - wordpress-install

License
-------

BSD

Author Information
------------------

J.L
