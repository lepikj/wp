# Playbook for installing appropriate database and wordpress

* Uses distribution-agnostic approach for database selection
* Two git repos used as submodules:
    https://github.com/geerlingguy/ansible-role-mysql.git
    https://github.com/sbaerlocher/ansible.wp-cli.git

## Usage

ansible-playbook [ -i hosts ] playbook.yaml

* vars/mysql.yaml  -- db settings
* vars/wordpress.yaml  -- wordpress initial setup data


